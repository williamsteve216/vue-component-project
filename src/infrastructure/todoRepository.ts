import { ITodosList } from "../domain/todo/entities";
import { ITodoRepository } from "../domain/todo/ports/secondary";

export class TodoRepository implements ITodoRepository {
	page?: number | undefined;
	pageSize?: number | undefined;
	constructor(page: number = 1, pageSize: number = 10) {
		this.page = page;
		this.pageSize = pageSize;
	}
	async getTodos(page?: number, pageSize?: number): Promise<ITodosList> {
		const baseURL: string = "https://jsonplaceholder.typicode.com/todos";
		const url: string = `${baseURL}?page=${page ?? this.page}&limit=${pageSize ?? this.pageSize}`;
		const res = await fetch(url);
		return res.json();
	}
}
