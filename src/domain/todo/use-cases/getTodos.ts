import { TodoRepository } from "../../../infrastructure/todoRepository";
import { IGetTodos } from "../ports/primaries";
import { TodoActionTypes } from "./todo.enum";
export class GetTodos implements IGetTodos {
	private _todoRepository: TodoRepository;
	constructor(todoRepository: TodoRepository) {
		this._todoRepository = todoRepository;
	}
	execute(page?: number, pageSize?: number): (dispatch: any) => Promise<void> {
		return async (dispatch) => {
			const { GET_TODOS_REQUEST, GET_TODOS_SUCCESS, GET_TODOS_FAIL } = TodoActionTypes;
			dispatch({
				type: GET_TODOS_REQUEST,
			});
			try {
				const todos = await this._todoRepository.getTodos(page, pageSize);
				dispatch({
					type: GET_TODOS_SUCCESS,
					payload: todos,
				});
			} catch (error) {
				dispatch({
					type: GET_TODOS_FAIL,
					error,
				});
			}
		};
	}
}
