import { ITodosList } from "../../entities";

export interface ITodoRepository {
	page?: number;
	pageSize?: number;

	getTodos(): Promise<ITodosList>;
}
