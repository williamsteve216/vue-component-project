export interface IGetTodos {
	execute(page?: number, pageSize?: number): (dispatch: any) => Promise<void>;
}
