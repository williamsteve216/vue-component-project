import { ITodoInput } from "../../entities";

export interface ICreateTodo {
	execute(todoInput?: ITodoInput): (dispatch: any) => Promise<void>;
}
