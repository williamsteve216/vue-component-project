export interface ITodo {
	userId: number;
	id: number;
	title: string;
	completed: boolean;
}

export interface ITodoInput {
	userId: number;
	title: string;
}

export interface ITodosList {
	results: ITodo[];
	info: IInfoList;
}

export interface IInfoList {
	page: number;
	pageSize: number;
	totalElement: number;
}
