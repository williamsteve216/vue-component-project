import { TodoRepository } from "../../../infrastructure";
import { GetTodos } from "../../../domain/todo/use-cases";
export default {
	namespaced: true,
	state: {
		todos: [],
		isLoading: false,
		error: {
			message: "",
		},
	},
	getters: {},
	mutations: {
		GET_TODOS_REQUEST: (state, payload) => {
			state.isLoading = true;
			console.log(payload);
		},
		GET_TODOS_SUCCESS: (state, payload) => {
			state.todos = payload.payload;
		},
	},
	actions: {
		getTodos: async (context, payload) => {
			const todoRepository = new TodoRepository();
			let getTodos = new GetTodos(todoRepository);
			try {
				const mutate = await getTodos.execute(payload.page, payload.pageSize);
				mutate(context.commit);
			} catch (error) {
				console.log(error);
			}
		},
	},
};
