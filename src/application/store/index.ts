import { createStore } from "vuex";
import todo from "./todo/todo.store";

export default createStore({
	modules: {
		todo,
	},
});
