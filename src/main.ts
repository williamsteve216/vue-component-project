import { createApp } from "vue";
import "./application/index.scss";
import "./application/style.css";
import App from "./application/App.vue";
import store from "./application/store";

createApp(App).use(store).mount("#app");
